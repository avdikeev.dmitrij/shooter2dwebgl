const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
  entry: './src/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(glsl|png|jpg)$/i,
        use: 'raw-loader',
      },
      {
        test: /\.vue$/,
        use: 'vue-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': path.resolve('src'),
    },
  },
  devServer: {
    historyApiFallback: {
      index: __dirname + '/dist/index.html',
    },
    contentBase: __dirname + '/dist'
  },
  plugins: [
    new CopyWebpackPlugin({
        patterns: [
            { from: 'src/assets', to: 'assets' },
        ]
    }),
    new HtmlWebpackPlugin ({
      inject: true,
      template: __dirname + '/index.html'
    }),
    new VueLoaderPlugin(),
  ]
};