const loadTexture = (src) => {
    const image = new Image();

    return new Promise((resolve, reject) => {
        image.onload = () => {
            resolve(image);
        };

        image.onerror = reject;

        image.src = src;
    });
};

export const loadTextures = () => {
    return new Promise(async (resolve, reject) => {
        const textures = [];

        const req = require.context("@/assets/textures", true,  /\.(png|jpg)$/);
        const keys = req.keys();
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            try {
                const texture = await loadTexture(`assets/textures/${key}`);
                textures.push(texture);
            } catch (e) {
                reject(e);
            }
        }
    
        resolve(textures);
    });
};
