export const loadMap = (name) => {
    const req = require.context("@/assets/maps", true,  /\.(json)$/);

    const hasMap = req.keys().map(_ => _.match(/\.\/(.+)\.json$/)[1]).includes(name);

    if (!hasMap) return false;

    const map = require(`@/assets/maps/${name}.json`);
    return map;
};
