import { loadTexture } from '../textures/index';
import glFunctions from '../utils/gl-functions';

import vertShader from './shaders/map.vert.glsl';
import fragShader from './shaders/map.frag.glsl';

export default class Map {
    constructor(gl, aspect, textures) {
        this.gl = gl;

        this.aspect = aspect;

        this.blockSize = 1;

        this.cameraPosition = [0.0, 0.0];

        this.util = glFunctions(this.gl);

        this.program = this.util.createProgram(vertShader, fragShader);

        this.textures = textures.map(texture => {
            return this.util.createTexture(gl.LINEAR, texture, texture.width, texture.height);
        });
    }

    get cameraX() {
        return this.cameraPosition[0];
    }

    get cameraY() {
        return this.cameraPosition[1];
    }

    set cameraX(x) {
        this.cameraPosition[0] = x;
    }

    set cameraY(y) {
        this.cameraPosition[1] = y;
    }
    
    setCameraPosition(x, y) {
        this.cameraPosition[0] = x;
        this.cameraPosition[1] = y;
    }

    drawBlock(x, y) {
        const { 
            cameraPosition, 
            util,
            gl,
            program,
        } = this;

       const verticies = [
            0.0, 0.0,
            1.0, 1.0,
            1.0, 0.0,
        
            0.0, 0.0,
            0.0, 1.0,
            1.0, 1.0,
        ];
    
        const u_translate = [2.0 * x, 2.0 * y];
    
        const buffer = util.createBuffer(gl.ARRAY_BUFFER, new Float32Array(verticies));
    
        gl.uniform2fv(program.u_translate, u_translate);
        gl.uniform2fv(program.u_camera, cameraPosition);
        gl.uniform1f(program.aspect, this.aspect);

        gl.uniform1i(program.uSampler, 0);
    
        util.bindAttribute(gl.ARRAY_BUFFER, buffer, program.a_pos, 2);
    
        gl.drawArrays(gl.TRIANGLES, 0, 6);
    }

    draw() {
        this.gl.useProgram(this.program.program);
        this.util.bindTexture(this.textures[0], 0);
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 1; j++) {
                this.drawBlock(i, j);
            }
        }
    }
}