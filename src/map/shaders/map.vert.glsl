attribute vec2 a_pos;

varying vec2 pos;

uniform vec2 u_translate;
uniform vec2 u_camera;
uniform float aspect;
		
void main() {
    pos = a_pos;
    vec2 p = a_pos * 2.0 - vec2(1.0) + u_translate - u_camera;

    mat2 a = mat2(
        0.1, 0.0,
        0.0, 0.1 * aspect
    );

    gl_Position = vec4(a * p, 0.0, 1.0);
}