import { loadTextures } from './textures/index';
import { loadMap } from './map/loader';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import Vue from 'vue';
import App from './views/App.vue';

// loadMap('de_dust');

import Map from './map/index';

new Vue({
    el: '#app',
    render: h => h(App),
});

const container = document.getElementById('container');

container.width = window.innerWidth;
container.height = window.innerHeight;

const gl = container.getContext('webgl2');

const aspect = container.width / container.height;

let map;

loadTextures().then((textures) => {
    map = new Map(gl, aspect, textures);
});
// GameLoop
setInterval(() => {
    gl.viewport(0, 0, container.width, container.height);

    if (map) {
        map.draw();
    }
}, 10);